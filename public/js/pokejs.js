function getJson() {
    let requete = "http://localhost:3000/pokemons/";
    fetch(requete)
        .then(response => {
                if (response.ok) return response.json();
                else throw new Error('Ajax error: ' + response.status);
            }
        )
        .then(fillPokemons)
        .then(clearMainError)
        .catch(function (error) {
            document.getElementById("main-error").innerHTML = "<b>Error while loading Pokémons: </b>" + error;
        });
}

function clearMainError() {
    document.getElementById("main-error").innerHTML = '';
}

function hideModal() {
    document.getElementById("modal").style.display = "none";

    resetFields();
}

function showEditor(pokemon) {
    document.getElementById("modal-label").innerText = "Edit a Pokémon";
    document.getElementById("modal-validate").onclick = editPokemon;

    document.getElementById("id").setAttribute("readonly", '');

    document.getElementById("id").value = pokemon["id"];
    document.getElementById("english").value = pokemon["english_name"];
    document.getElementById("japanese").value = pokemon["japanese_name"];
    document.getElementById("chinese").value = pokemon["chinese_name"];
    document.getElementById("type").value = pokemon["type"];

    document.getElementById("modal").style.display = "block";
}

function showGenerator() {
    document.getElementById("modal-label").innerText = "Add a Pokémon";
    document.getElementById("modal-validate").onclick = addPokemon;

    resetFields();

    document.getElementById("id").removeAttribute("readonly");

    document.getElementById("modal").style.display = "block";
}

function resetFields() {
    document.getElementById("id").value = '';
    document.getElementById("english").value = '';
    document.getElementById("english").className = "form-control col-8";
    document.getElementById("japanese").value = '';
    document.getElementById("chinese").value = '';
    document.getElementById("type").value = '';
    document.getElementById("modal-error").innerText = '';
}

function fillPokemons(pokemons) {
    let ul = document.getElementById("pokemons");
    ul.innerHTML = '';
    if (pokemons.length === 0) {
        let p = document.createElement("h1");
        p.innerText = "No Pokémon found.";
        p.className = "text-light";
        ul.appendChild(document.createElement("li").appendChild(p));
    }
    for (let i = 0; i < pokemons.length; i++) {
        let li = document.createElement("li");

        let button = document.createElement("button");
        button.type = "button";
        button.role = "button";
        button.innerText = pokemons[i]["english_name"];
        button.addEventListener('click', function () {
            showEditor(pokemons[i]);
        });
        button.className = "btn btn-lg btn-light";
        li.appendChild(button);

        let edit = document.createElement("button");
        edit.type = "button";
        edit.role = "button";
        edit.innerText = "✎";
        edit.addEventListener('click', function () {
            showEditor(pokemons[i]);
        });
        edit.className = "btn btn-lg btn-primary flex-grow-0 flex-shrink-0";
        li.appendChild(edit);

        let del = document.createElement("button");
        del.type = "button";
        del.role = "button";
        del.innerText = "×";
        del.addEventListener('click', function () {
            deletePokemon(pokemons[i]["id"]);
        });
        del.className = "btn btn-lg btn-danger flex-grow-0 flex-shrink-0";
        li.appendChild(del);

        li.style.margin = "0.75rem";
        li.className = "btn-group";
        li.role = "group";
        ul.appendChild(li);
    }
}

function addPokemon() {
    let requete = "http://localhost:3000/pokemons/?english_name=" + document.getElementById("english").value;
    fetch(requete)
        .then(response => {
            if (response.ok) return response.json();
            else throw new Error('Ajax error: ' + response.status);
        })
        .then(applyAdd)
        .catch(function (error) {
            document.getElementById("english").className = "form-control col-8 is-invalid";
            document.getElementById("modal-error").innerText = error.message;
        })
}

function applyAdd(response) {
    if (response.length !== 0)
        throw new Error('Insert failed, duplicate english name');

    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("dataType", "json");

    let id = document.getElementById("id").value;

    let values = [id, document.getElementById("english").value,
        document.getElementById("japanese").value,
        document.getElementById("chinese").value,
        document.getElementById("type").value];

    if (values.every(function (element) {
        return element !== "";
    })) {
        let myInit = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                id: values[0],
                english_name: values[1],
                japanese_name: values[2],
                chinese_name: values[3],
                type: values[4]
            })
        };

        let myRequest = new Request("http://localhost:3000/pokemons", myInit);

        fetch(myRequest, myInit)
            .then(response => {
                if (response.ok) getJson();
                else throw new Error('Ajax error: ' + response.status);
            });
        hideModal();
    }
}

function editPokemon() {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("dataType", "json");


    let values = [id, document.getElementById("english").value,
        document.getElementById("japanese").value,
        document.getElementById("chinese").value,
        document.getElementById("type").value];

    if (values.every(function (element) {
        return element !== "";
    })) {

        let myInit = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify({
                id: document.getElementById("id").value,
                english_name: document.getElementById("english").value,
                japanese_name: document.getElementById("japanese").value,
                chinese_name: document.getElementById("chinese").value,
                type: document.getElementById("type").value
            })
        };

        let myRequest = new Request("http://localhost:3000/pokemons/" + document.getElementById("id").value, myInit);

        fetch(myRequest, myInit)
            .then(response => {
                if (response.ok) getJson();
                else throw new Error('Ajax error: ' + response.status);
            });
        hideModal();
    }
}

function deletePokemon(pokemon_id) {
    let myRequest = new Request("http://localhost:3000/pokemons/" + pokemon_id, {method: "DELETE"});
    fetch(myRequest)
        .then(response => {
            if (response.ok) getJson();
            else throw new Error('Ajax error: ' + response.status);
        })
        .then(clearMainError)
        .catch(function (error) {
            document.getElementById("main-error").innerHTML = "<b>Error while deleting a Pokémon: </b>" + error;
        });
}

function searchPokemon() {
    if (document.getElementById("search").value !== "") {
        let requete = "http://localhost:3000/pokemons/?q=" + document.getElementById("search").value;
        fetch(requete)
            .then(response => {
                    if (response.ok) return response.json();
                    else throw new Error('Ajax error: ' + response.status);
                }
            )
            .then(fillPokemons)
            .then(clearMainError)
            .catch(function (error) {
                document.getElementById("main-error").innerHTML = "<b>Error while searching Pokémons: </b>" + error;
            });
    }
}